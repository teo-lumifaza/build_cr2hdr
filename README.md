# build_cr2hdr

Script for building cr2hdr on modern Unix systems. Tested on Ubuntu Linux.

**cr2hdr** is a postprocessing tool for photographs taken with [Magic Lantern](https://www.magiclantern.fm/), [Dual ISO](https://www.magiclantern.fm/forum/index.php?topic=7139.0) module.

The problem is that it isn't packaged for Linux systems and building requires Python2 and ARM compiler (to satisfy build scripts, it isn't used anyway). This tool bypasses this requirement by patching Magic Lantern source code.

System requirements:
* C compiler
* `hg` (Mercurial) or `wget` for fetching source code

